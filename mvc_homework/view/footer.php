<footer class="page-footer">
            <div class="container">
                <div class="row">
                    <div class="col l6 s12">
                        <h1 class="white-text">ThuStore</h1>
                        <p class="grey-text text-lighten-4">Specializing in providing gaming products.</p>
                    </div>
                    <div class="col l4 offset-l1 s12">
                        <h5 class="white-text">Help</h5>
                        <ul>
                            <li><a class="grey-text text-lighten-3" href="#!">Payment</a></li> <br>
                            <li><a class="grey-text text-lighten-3" href="#!">Shipping</a></li> <br>
                            <li><a class="grey-text text-lighten-3" href="#!">Cancellation</a></li> <br>
                            <li><a class="grey-text text-lighten-3" href="#!">Returns</a></li>
                        </ul>
                    </div>
                    <div class="col l4 offset-l2 s12">
                        <h5 class="white-text">Policy</h5>
                        <ul>
                            <li><a class="grey-text text-lighten-3" href="#!">Return</a></li> <br>
                            <li><a class="grey-text text-lighten-3" href="#!">Security</a></li> <br>
                            <li><a class="grey-text text-lighten-3" href="#!">Privacy</a></li>
                        </ul>
                    </div>
                    <div class="col l4 offset-l2 s12">
                        <h5 class="white-text">Social</h5>
                        <ul >
                            <li><a class="grey-text text-lighten-3" href="#!"><i class="fab fa-facebook-square"></i> Facebook</a></li> <br>
                            <li><a class="grey-text text-lighten-3" href="#!"><i class="fab fa-google"></i> Google</a></li> <br>
                            <li><a class="grey-text text-lighten-3" href="#!"><i class="fab fa-instagram"></i> Instagram</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container"> © 2019 Copyright BBBootstrap.com <a class="grey-text text-lighten-4 right" href="#!">House of cool code snippet!</a> </div>
            </div>
        </footer>





        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    $(this).toggleClass('active');
                });
            });
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </body>
</html>
