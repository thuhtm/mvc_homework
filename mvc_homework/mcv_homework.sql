-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th9 14, 2020 lúc 05:13 AM
-- Phiên bản máy phục vụ: 10.4.8-MariaDB
-- Phiên bản PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `mcv_homework`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(255) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `image`, `description`) VALUES
(1, 'Laptop Asus', 2800, 'laptop_asus_GX531.jpg', 'Laptop gaming siêu mỏng được trang bị màn hình cảm ứng phụ 14.1”, giúp hỗ trợ đa nhiệm, chơi game, sáng tạo nội dung và hơn thế nữa'),
(2, 'Chuột Dare-U Gaming', 350, 'Chuot_gaming_VT200.jpg', 'ROG Strix Impact là mẫu chuột nhẹ, thuận hai tay mang lại hiệu suất cần thiết cho bạn để thống lĩnh các tựa game MOBA yêu thích. Với các nút điều khiển siêu nhạy, chuyển động nhanh cùng với độ chính xác tuyệt vời kết hợp với thiết kế thoải mái.'),
(3, 'Bàn Phím Gaming', 1350, 'banphim_gaming.jpg', 'ROG Strix Scope là mẫu bàn phím cơ với công nghệ phím bấm Xccurate Ctrl key với kích thước lớn, tăng độ chính xác cho các game FPS, công nghệ phím \"tàng hình\" một chạm và các phím điều khiển nhanh cho media và các phím chức năng khác..'),
(4, 'Headphone Gaming', 1100, 'headphone_gaming.jpg', 'HyperX là một cái tên quá quen thuộc với những bộ Gaming gear nổi tiếng, điển hình là những tai nghe của HyperX đang làm mưa làm gió trong thị trường game và được nhiều game thủ biết đến.'),
(5, 'Miếng Lót Chuột', 18, 'lotchuot.jpg', 'Miếng lót chuột cao cấp');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
