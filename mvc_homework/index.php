<?php
    include "model/pdo.php";
    include "model/product.php";

    $dspro=loadproducts();

    //1. Header
    include "view/header.php";

    //2. Body
    $ctr="home";
    if(isset($_GET['ctrller'])){
        $ctr=$_GET['ctrller'];
        switch ($ctr) {
            case 'detail':
                if(isset($_GET['id'])){
                    $id=$_GET['id'];
                    $prodetail=productdetail($id);
                    
                    include "view/productdetail.php";
                }else{
                    include 'controller/'.$ctr.'Controller.php';
                }
            break;

            default:
                include 'controller/'.$ctr.'Controller.php'; 
            break;
        }
    }else{
        include 'controller/'.$ctr.'Controller.php'; //controller/homeController.php
    }

    //3. Footer
    include "view/footer.php"; 
?>